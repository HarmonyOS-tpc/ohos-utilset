package com.navercorp.utilset.input;

import ohos.app.Context;

/**
 *
 * @author jaemin.woo
 *
 */
class SoftwareKeyDetector {
    private boolean softkeyNavigationCalled;

    private boolean softkeyNavigationBar;

    /**
     * hasSoftwareKeys
     * @param context Context
     * @return true
     */
    public boolean hasSoftwareKeys(Context context) {
        return true;
    }

    /**
     * getHasSoftkeyNavigationBar
     *
     * @return true or false
     */
    public boolean getHasSoftkeyNavigationBar() {
        // Prevent reflection from being called indefinitely
        if (softkeyNavigationCalled) {
            return softkeyNavigationBar;
        }
        softkeyNavigationCalled = false;
        return softkeyNavigationBar;
    }
}
