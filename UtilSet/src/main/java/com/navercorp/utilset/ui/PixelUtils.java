package com.navercorp.utilset.ui;

import ohos.agp.window.service.DisplayAttributes;

/**
 *
 * @author jaemin.woo
 *
 */
public class PixelUtils {
    private static final float ROUND_FACTOR = 0.5f;

    private PixelUtils() {
        /* Do nothing */
    }

    /**
     * getDpFromPixel
     * Converts Pixel to DP<br>
     *
     * @param pixel Pixel
     * @return DP
     */
    public static int getDpFromPixel(int pixel) {
        DisplayAttributes displayAttributes = new DisplayAttributes();
        float scale = displayAttributes.densityPixels;
        return (int) (pixel / scale);
    }

    /**
     * getPixelFromDp
     * Converts DP to Pixel<br>
     *
     * @param dp DP
     * @return Pixel
     */
    public static int getPixelFromDp(int dp) {
        // Get the screen's density scale
        DisplayAttributes displayAttributes = new DisplayAttributes();
        float scale = displayAttributes.densityPixels;

        // Convert the dps to pixels, based on density scale
        return (int) (dp * scale + ROUND_FACTOR);
    }
}
