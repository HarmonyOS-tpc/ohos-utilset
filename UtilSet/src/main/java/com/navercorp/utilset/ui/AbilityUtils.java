package com.navercorp.utilset.ui;

import ohos.aafwk.ability.AbilityMissionInfo;
import ohos.aafwk.ability.RunningProcessInfo;

import ohos.app.Context;

import ohos.bundle.ElementName;

import java.util.List;

/**
 *
 * @author jaemin.woo
 *
 */
public class AbilityUtils {
    private AbilityUtils() {
        /* Do nothing */
    }

    /**
     * Checks if a package is installed.
     *
     * @param context Context to be used to verify the existence of the package.
     * @param packageName Package name to be searched.
     * @return true if the package is discovered; false otherwise
     */
    public static boolean isPackageInstalled(Context context, String packageName) {
        try {
            context.getBundleManager().getApplicationInfo(packageName, 0, 0);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns Package name of base ability.<p>
     * Requires GET_TASK permission
     *
     * @param context Context to get base ability information
     * @return String containing base package name
     */
    public static String getBaseAbilityPackageName(Context context) {
        List<AbilityMissionInfo> abilityMissionInfoList = context.getAbilityManager().queryRunningAbilityMissionInfo(1);
        ElementName baseAbility = getBaseAbility(abilityMissionInfoList);
        if (baseAbility == null) {
            return null;
        }
        return baseAbility.getBundleName();
    }

    /**
     * Returns Class name of base ability
     *
     * @param context Context to provide ability information
     * @return String representing class name of base ability
     */
    public static String getBaseAbilityClassName(Context context) {
        List<AbilityMissionInfo> abilityMissionInfoList = context.getAbilityManager().queryRunningAbilityMissionInfo(1);
        ElementName ability = getBaseAbility(abilityMissionInfoList);
        if (ability == null) {
            return null;
        }
        return ability.getAbilityName();
    }

    /**
     * Returns Package name of top ability
     *
     * @param context Context
     * @return String representing package name of top ability
     */
    public static String getTopAbilityPackageName(Context context) {
        List<AbilityMissionInfo> abilityMissionInfoList = context.getAbilityManager().queryRunningAbilityMissionInfo(1);
        ElementName ability = getTopAbility(abilityMissionInfoList);
        if (ability == null) {
            return null;
        }
        return ability.getBundleName();
    }

    /**
     * Returns Class name of top ability
     *
     * @param context Context
     * @return String representing class name of top ability
     */
    public static String getTopAbilityClassName(Context context) {
        List<AbilityMissionInfo> abilityMissionInfoList = context.getAbilityManager().queryRunningAbilityMissionInfo(1);
        ElementName ability = getTopAbility(abilityMissionInfoList);
        if (ability == null) {
            return null;
        }
        return ability.getAbilityName();
    }

    /**
     * Determines if this application is top ability
     *
     * @param context Context to be examined
     * @return true if this application is a top ability; false otherwise
     */
    public static boolean isTopApplication(Context context) {
        List<AbilityMissionInfo> abilityMissionInfoList = context.getAbilityManager().queryRunningAbilityMissionInfo(1);
        ElementName ability = getTopAbility(abilityMissionInfoList);
        if (ability == null) {
            return false;
        }
        return ability.getBundleName().equals(context.getApplicationInfo().getName());
    }

    /**
     * Checks if this application is foreground
     *
     * @param context Context to be examined
     * @return true if this application is running on the top; false otherwise
     */
    public static boolean isContextForeground(Context context) {
        List<RunningProcessInfo> appProcesses = context.getAbilityManager().getAllRunningProcesses();
        int pid = ohos.os.ProcessManager.getPid();
        for (RunningProcessInfo appProcess : appProcesses) {
            if (appProcess.getPid() == pid) {
                return appProcess.getWeight() == RunningProcessInfo.WEIGHT_FOREGROUND;
            }
        }
        return false;
    }

    private static ElementName getTopAbility(List<AbilityMissionInfo> abilityMissionInfoList) {
        return abilityMissionInfoList.get(0).getAbilityTopBundleName();
    }

    private static ElementName getBaseAbility(List<AbilityMissionInfo> abilityMissionInfoList) {
        return abilityMissionInfoList.get(0).getAbilityBaseBundleName();
    }
}
