package com.navercorp.utilset.cipher;

/**
 * @author jaemin.woo
 */
public interface CipherObject {
    void encrypt(String seed, String plainText);

    void decrypt(String seed, String cipherText);
}
