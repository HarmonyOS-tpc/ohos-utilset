package com.navercorp.utilset.device;

/**
 * Function description
 * enum LauncherType
 */
public enum LauncherType {
    /**
     * Default Launcher
     */
    OHOS("ohos", 130);

    String packageName;

    float paddingDp;

    LauncherType(String packageName, float paddingDp) {
        this.packageName = packageName;
        this.paddingDp = paddingDp;
    }

    /**
     * getPaddingDp
     * @return paddingDp
     */
    public float getPaddingDp() {
        return paddingDp;
    }
}
