package com.navercorp.utilset.device;
/*
 * DeviceInfoHelper.java
 *
 * Copyright 2011 NHN Corp. All rights Reserved.
 * NHN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

import ohos.app.Context;

/**
 * Function description
 * class DeviceTypeDetector
 */
public class DeviceTypeDetector {
    /**
     * getDeviceType
     * Checks if the device is a tablet or a phone
     *
     * @param context Context
     * @return DeviceType
     */
    public DeviceType getDeviceType(Context context) {
        int deviceType = context.getResourceManager().getDeviceCapability().deviceType;
        // If deviceType is 3 ,Tablet.
        if (deviceType == ohos.global.configuration.DeviceCapability.DEVICE_TYPE_TABLET) {
            return DeviceType.Tablet;
        }
        return DeviceType.Handset;
    }
}
