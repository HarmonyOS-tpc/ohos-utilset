package com.navercorp.utilset.device;

import ohos.app.Context;

import ohos.telephony.SimInfoManager;

/**
 * Function description
 * class PhoneNumberUtils
 */
public class PhoneNumberUtils {
    /**
     * isAbleToReceiveSms
     *
     * @param context Context
     * @return boolean
     */
    public boolean isAbleToReceiveSms(Context context) {
        SimInfoManager simInfoManager = SimInfoManager.getInstance(context);
        String phoneNumber = simInfoManager.getSimTelephoneNumber(simInfoManager.getDefaultVoiceSlotId());
        if (phoneNumber == null) {
            return false;
        }
        return phoneNumber.length() != 0;
    }
}
