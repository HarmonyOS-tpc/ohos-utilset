package com.navercorp.utilset.device;

import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jaemin.woo
 */
public class LauncherTypeDetector {
    private static List<String> exceptDevices = new ArrayList<>();

    static {
        exceptDevices.add("LIO-AL00");
    }

    private LauncherTypeDetector() {
        /* Do nothing */
    }

    private static boolean containExceptDevices() {
        String device = ohos.system.DeviceInfo.getName();
        return exceptDevices.contains(device);
    }

    /**
     * getType
     * @param context Context
     * @return LauncherType
     */
    public static LauncherType getType(Context context) {
        String packageName = LauncherInfo.getName(context);
        if (packageName.length() == 0) {
            return LauncherType.OHOS;
        }

        if (containExceptDevices()) {
            return LauncherType.OHOS;
        }

        for (LauncherType type : LauncherType.values()) {
            if (packageName.contains(type.packageName)) {
                return type;
            }
        }
        return LauncherType.OHOS;
    }
}
