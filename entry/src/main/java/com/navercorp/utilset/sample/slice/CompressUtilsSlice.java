/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.string.CompressUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

/**
 * Function description
 * class CompressUtilsSlice
 */
public class CompressUtilsSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_compress_utils_ability_slice);

        initializeViews();
    }

    private void initializeViews() {
        TextField compressText = (TextField) findComponentById(ResourceTable.Id_compress_text);

        Text compressedString = (Text) findComponentById(ResourceTable.Id_compressed_string);

        Button submit = (Button) findComponentById(ResourceTable.Id_submit);
        submit.setClickedListener(component -> {
            String orgString = compressText.getText();
            String result = CompressUtils.compressString(orgString);
            compressedString.setText(result);
        });
    }
}
