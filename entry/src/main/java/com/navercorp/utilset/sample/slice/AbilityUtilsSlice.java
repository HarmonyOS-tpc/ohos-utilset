/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.sample.UiAbility;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * Function description
 * class AbilityUtilsSlice
 */
public class AbilityUtilsSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_ui_utils_slice);
        initializeViews();
    }

    private void initializeViews() {
        Intent intent = new Intent();

        Button keepScreenOn = null;
        Component buttonComponent = findComponentById(ResourceTable.Id_keep_screen_on);
        if (buttonComponent instanceof Button) {
            keepScreenOn = (Button) buttonComponent;
        }
        keepScreenOn.setClickedListener(component -> {
            Operation systemOperation = new Intent.OperationBuilder().withAction("action.KeepScreenOnSlice")
                .withBundleName(getBundleName())
                .withAbilityName(UiAbility.class.getSimpleName())
                .build();
            intent.setOperation(systemOperation);
            startAbility(intent);
        });

        Button clearScreenOn = null;
        buttonComponent = findComponentById(ResourceTable.Id_clear_screen_on);
        if (buttonComponent instanceof Button) {
            clearScreenOn = (Button) buttonComponent;
        }
        clearScreenOn.setClickedListener(component -> {
            Operation systemOperation = new Intent.OperationBuilder().withAction("action.ClearScreenOnSlice")
                .withBundleName(getBundleName())
                .withAbilityName(UiAbility.class.getSimpleName())
                .build();
            intent.setOperation(systemOperation);
            startAbility(intent);
        });
    }
}
