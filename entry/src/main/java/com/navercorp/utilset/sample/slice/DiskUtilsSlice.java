/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.storage.DiskUtils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Component;
import ohos.agp.components.Text;

/**
 * Function description
 * class DiskUtilsSlice
 */
public class DiskUtilsSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_disk_utils_ability_slice);

        initializeViews();
    }

    private void initializeViews() {
        Component textComponent = findComponentById(ResourceTable.Id_is_external_storage_mounted);
        if (textComponent instanceof Text) {
            Text isExternalStorageMounted = (Text) textComponent;
            isExternalStorageMounted.setText(String.valueOf(DiskUtils.isExternalStorageMounted()));
        }

        textComponent = findComponentById(ResourceTable.Id_get_external_directory_path);
        if (textComponent instanceof Text) {
            Text getExternalDirectoryPath = (Text) textComponent;
            getExternalDirectoryPath.setText(DiskUtils.getExternalDirPath(getContext()));
        }

        textComponent = findComponentById(ResourceTable.Id_get_external_temp_directory_path);
        if (textComponent instanceof Text) {
            Text getExternalTempDirectoryPath = (Text) textComponent;
            getExternalTempDirectoryPath.setText(DiskUtils.getExternalTemporaryDirPath(getContext()));
        }

        textComponent = findComponentById(ResourceTable.Id_get_external_context_root_dir);
        if (textComponent instanceof Text) {
            Text getExternalContextRootDir = (Text) textComponent;
            getExternalContextRootDir.setText(DiskUtils.getExternalContextRootDir(getContext()));
        }
    }
}
