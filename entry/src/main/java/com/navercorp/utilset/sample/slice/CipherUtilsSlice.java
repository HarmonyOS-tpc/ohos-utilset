/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample.slice;

import com.navercorp.utilset.cipher.AesCipher;
import com.navercorp.utilset.cipher.CipherUtils;
import com.navercorp.utilset.sample.ResourceTable;
import com.navercorp.utilset.sample.util.LogUtil;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Function description
 * class CipherUtilsSlice
 */
public class CipherUtilsSlice extends AbilitySlice {
    private static final String TAG = CipherUtilsSlice.class.getSimpleName();

    private TextField seedText;

    private TextField valueText;

    private Text encryptedText;

    private Text decryptedText;

    private String seed;

    private String value;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_cipher_utils_ability_slice);

        initializeViews();
    }

    private void initializeViews() {
        Component textComponent = findComponentById(ResourceTable.Id_seed);
        if (textComponent instanceof TextField) {
            seedText = (TextField) textComponent;
        }

        textComponent = findComponentById(ResourceTable.Id_text);
        if (textComponent instanceof TextField) {
            valueText = (TextField) textComponent;
        }

        textComponent = findComponentById(ResourceTable.Id_encrypted_text);
        if (textComponent instanceof Text) {
            encryptedText = (Text) textComponent;
        }

        textComponent = findComponentById(ResourceTable.Id_decrypted_text);
        if (textComponent instanceof Text) {
            decryptedText = (Text) textComponent;
        }

        CipherUtils cipherUtils = new CipherUtils();

        Button encrypt = (Button) findComponentById(ResourceTable.Id_encrypt);
        encrypt.setClickedListener(component -> {
            seed = seedText.getText();
            value = valueText.getText();

            if (seed.length() > 0 && value.length() > 0) {
                cipherUtils.encrypt(seed, value);
                encryptedText.setText(AesCipher.getsEncryptedText());
            } else {
                showMessage(ResourceTable.String_encryption_warning);
            }
        });

        Button decrypt = (Button) findComponentById(ResourceTable.Id_decrypt);
        decrypt.setClickedListener(component -> {
            seed = seedText.getText();
            value = encryptedText.getText();

            if (value.length() > 0) {
                cipherUtils.decrypt(seed, value);
                decryptedText.setText(AesCipher.getsDecryptedText());
            } else {
                showMessage(ResourceTable.String_decryption_warning);
            }
        });
    }

    private void showMessage(int resourceId) {
        String decryptionWarning = null;
        try {
            decryptionWarning = getResourceManager().getElement(resourceId).getString();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.info(TAG, "Exception");
        }
        ToastDialog toastDialog = new ToastDialog(getContext());
        toastDialog.setText(decryptionWarning);
        toastDialog.show();
    }
}
