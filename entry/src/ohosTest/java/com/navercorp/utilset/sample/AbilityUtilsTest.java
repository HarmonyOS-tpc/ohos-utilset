/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.navercorp.utilset.sample.MainAbility;
import com.navercorp.utilset.ui.AbilityUtils;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import ohos.app.Context;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;

/**
 * Function description
 * class AbilityUtilsTest
 */
@RunWith(JUnitParamsRunner.class)
public class AbilityUtilsTest {
    private Iterable getContextTest() {
        return Collections.singleton(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
    }

    /**
     * Returns Package name of top ability
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void isPackageInstalledTest(Context context) {
        assertTrue(AbilityUtils.isPackageInstalled(context, "com.navercorp.utilset.sample"));
    }

    /**
     * Returns Package name of top ability
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void getBaseAbilityPackageNameTest(Context context) {
        assertEquals("com.navercorp.utilset.sample", AbilityUtils.getBaseAbilityPackageName(context));
    }

    /**
     * getBaseAbilityClassNameTest
     */
    @Test
    public void getBaseAbilityClassNameTest() {
        MainAbility mainAbility = new MainAbility();
        assertEquals("com.navercorp.utilset.sample.MainAbility",
                AbilityUtils.getBaseAbilityClassName(mainAbility.getContext()));
    }

    /**
     * getTopAbilityPackageNameTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void getTopAbilityPackageNameTest(Context context) {
        assertEquals("com.navercorp.utilset.sample", AbilityUtils.getTopAbilityPackageName(context));
    }

    /**
     * getTopAbilityClassNameTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void getTopAbilityClassNameTest(Context context) {
        assertEquals("com.navercorp.utilset.sample.UiAbility",
                AbilityUtils.getTopAbilityClassName(context));
    }

    /**
     * isTopApplicationThisTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void isTopApplicationThisTest(Context context) {
        assertFalse(AbilityUtils.isTopApplication(context));
    }

    /**
     * isContextForegroundThisTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void isContextForegroundThisTest(Context context) {
        assertTrue(AbilityUtils.isContextForeground(context));
    }

    /**
     * isTopApplicationTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void isTopApplicationTest(Context context) {
        assertFalse(AbilityUtils.isTopApplication(context));
    }

    /**
     * isContextForegroundTest
     *
     * @param context Context
     */
    @Test
    @Parameters(method = "getContextTest")
    public void isContextForegroundTest(Context context) {
        assertTrue(AbilityUtils.isContextForeground(context));
    }
}
