/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.navercorp.utilset.sample;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.navercorp.utilset.network.NetworkMonitor;

import junitparams.JUnitParamsRunner;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Function description
 * class NetworkUtilsTest
 */
@RunWith(JUnitParamsRunner.class)
public class NetworkUtilsTest {
    private NetworkMonitor networkMonitor = null;

    /**
     * getNetworkMonitorData
     */
    @Before
    public void getNetworkMonitorData() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        networkMonitor = NetworkMonitor.getInstance(context);
    }

    /**
     * isWifiConnectedTest
     */
    @Test
    public void isWifiConnectedTest() {
        assertTrue(networkMonitor.isWifiConnected());
    }

    /**
     * isWifiEnabledTest
     */
    @Test
    public void isWifiEnabledTest() {
        assertTrue(networkMonitor.isWifiEnabled());
    }

    /**
     * isMobileConnectedTest
     */
    @Test
    public void isMobileConnectedTest() {
        assertFalse(networkMonitor.isMobileConnected());
    }

    /**
     * getMobileStateTest
     */
    @Test
    public void getMobileStateTest() {
        assertFalse(networkMonitor.getMobileState());
    }

    /**
     * isNetworkConnectedTest
     */
    @Test
    public void isNetworkConnectedTest() {
        assertTrue(networkMonitor.isNetworkConnected());
    }

    /**
     * isAirplaneModeOnTest
     */
    @Test
    public void isAirplaneModeOnTest() {
        assertFalse(networkMonitor.isAirplaneModeOn());
    }

    /**
     * getSimStateTest
     */
    @Test
    public void getSimStateTest() {
        assertEquals(0, networkMonitor.getSimState());
    }

    /**
     * getWifiConnectedPreviouslyTest
     */
    @Test
    public void getWifiConnectedPreviouslyTest() {
        assertTrue(networkMonitor.getWifiConnectedPreviously());
    }

    /**
     * getIpAddressVersionFourTest
     */
    @Test
    public void getIpAddressVersionFourTest() {
        assertEquals("xxx.xxx.x.x", networkMonitor.getIpAddress(false));
    }

    /**
     * getIpAddressVersionSixTest
     */
    @Test
    public void getIpAddressVersionSixTest() {
        assertEquals("xxxx::xxxx:xxxx:xxxx:xxxx", networkMonitor.getIpAddress(true));
    }

    /**
     * getWifiMacAddressTest
     */
    @Test
    public void getWifiMacAddressTest() {
        assertEquals("xx:xx:xx:xx:xx:xx", networkMonitor.getWifiMacAddress());
    }

    /**
     * addPhoneCalledListenerTest
     */
    @Test
    public void addPhoneCalledListenerTest() {
        NetworkMonitor.PhoneCalledListener phoneCalledListener = i -> {
        };
        phoneCalledListener.onPhoneCallStateChanged(1);
        assertTrue(networkMonitor.addPhoneCalledListener(phoneCalledListener));
    }
}
