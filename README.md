# ohos-utilset

ohos-utilset: ohos-utilset is a collection of useful functions to save your valuable time.Because there are a ton of different ways to implement a method,we often spend our valuable
time to search and test those ways.Some may work, but some may not work.As such, we collected, tested and refined methods so as to prevent seeking code snippet.

# ohos-utilset includes:

  * DeviceUtils
    + determines whether the device is mobile phone or not
    + determines if the phone has SMS capability

  * AbilityUtils
    + clear and keep screen on
    + checks if a user specified package is installed
    + gets information of base activity package name and class name

  * NetworkUtils
    + provides listeners which notify when WiFi state changes, , receives a call, or a WiFi / Mobile connection is made
    + checks if WiFi is enabled and if WiFi is connected
    + checks if Mobile Network (3G/4G) is connected
    + obtains IP Address in either v4 or v6 form

  * DiskUtils
    + obtains external storage path for caching
    + obtains external storage path for temporary files
    + obtains MicroSD Card path

  * CipherUtils
    + provides simple AES based cryptographic methods

  * StringUtils
    + provides string compression/decompression methods

  * SystemUtils
    + checks if a device is rooted
    + counts the number of processors(more specifically the number of cores)

# Usage Instructions
Sample application require following permissions:
```
ohos.permission.GET_WIFI_INFO
ohos.permission.GET_TELEPHONY_STATE
ohos.permission.GET_NETWORK_INFO
```

DeviceUtils:

Getting launcher type:
```
String launcher = String.valueOf(LauncherTypeDetector.getType(/* Pass Context*/));
```

Getting device type:
```
DeviceTypeDetector deviceTypeDetector = new DeviceTypeDetector();
String device = String.valueOf(deviceTypeDetector.getDeviceType(/* Pass Context*/));
```

AbilityUtils:

Keep screen on:
```
ScreenUtils.setScreenOn(/* Pass Ability*/);
```

Clear screen on:
```
ScreenUtils.clearScreenOn(/* Pass Ability*/);
```

Checking package installed:
```
boolean isPackageInstalled = AbilityUtils.isPackageInstalled(/* Pass Context*/, /* Pass Bundlename*/);
```

Getting BaseAbility Package name
```
String value = AbilityUtils.getBaseAbilityPackageName(/* Pass Context*/);
```

Getting BaseAbility Class name
```
String value = AbilityUtils.getBaseAbilityClassName(/* Pass Context*/);
```

Getting TopAbility Package name
```
String value = AbilityUtils.getTopAbilityPackageName(/* Pass Context*/);
```

Getting TopAbility Class name
```
String value =  AbilityUtils.getTopAbilityClassName(/* Pass Context*/);
```

Checking TopAbility status
```
boolean value = AbilityUtils.isTopApplication(/* Pass Context*/);
```

Checking Context foreground status
```
boolean value = AbilityUtils.isContextForeground(/* Pass Context*/);
```

NetworkUtils:

Getting Wi-fi connected status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.isWifiConnected();
```

Getting Wi-fi enabled status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.isWifiEnabled();
```

Getting mobile network connected status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.isMobileConnected();
```

Getting mobile network turned on/off status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.getMobileState();
```

Getting network currently connected status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.isNetworkConnected();
```

Getting airplane mode status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.isAirplaneModeOn();
```

Getting sim status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.getSimState();
```

Getting Wi-fi previously connected status
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
boolean value = networkUtils.getWifiConnectedPreviously();
```

Getting IPv6 and IPv4 based on boolean value(True/False) respectively
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
String value = networkUtils.getIpAddress(false);
```

Getting MAC Address of WiFi Adapter
```
NetworkMonitor networkUtils = NetworkMonitor.getInstance(/* Pass Context*/);
String value = networkUtils.getWifiMacAddress();
```

DiskUtils:

Getting External storage mounted status
```
boolean value = DiskUtils.isExternalStorageMounted();
```

Getting directory name to save cache data in the external storage
```
String directory = DiskUtils.getExternalDirPath(/* Pass Context*/);
```

Getting directory name to save temporary files in the external storage for temporary
```
String directory = DiskUtils.getExternalTemporaryDirPath(/* Pass Context*/);
```

Getting root directory of the external storage
```
String directory = DiskUtils.getExternalContextRootDir(/* Pass Context*/);
```

CipherUtils:

Perform encryption
```
CipherUtils cipherUtils = new CipherUtils();
cipherUtils.encrypt(/* Pass seed(key) for encryption*/, /* Pass value to encrypt*/);
```

Perform decryption
```
CipherUtils cipherUtils = new CipherUtils();
cipherUtils.decrypt(/* Pass seed(key) corresponding to the encrypted value*/, /* Pass encrypted value*/);
```

StringUtils:

Perform compression
```
String compressedString = CompressUtils.compressString(/* Pass string to compress*/);
```

SystemUtils:

Getting processor numbers
```
int processorNumber = SystemUtils.getProcessorNumbers();
```

Getting device rooted status
```
boolean value = SystemUtils.isRooted();
```

# Installation Instructions
1.For using ohos-utilset module in sample app,include the below library dependency to generate hap/utilset.har.

Modify entry build.gradle as below :
```
dependencies {
    implementation project(path: ':utilset')
    testImplementation 'junit:junit:4.12'
    testImplementation 'pl.pragmatists:JUnitParams:1.1.1'
}
```

2.For using ohos-utilset in separate application, add the "utilset.har" in libs folder of "entry" module.

Modify entry build.gradle as below :

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
}
```

3.For using ohos-utilset from remote repository in separate application, add the below dependency in "entry" build.gradle.

Modify entry build.gradle as below :

```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohos-utilset:1.0.1'
}
```

# License
```
Copyright 2013 Navercorp

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
